# Frontend boilerplate:
Frontend boilerplate to create faster projects with gulp, less, html includes and many more features.

For cloning the repository and installing all the dependencies execute:
```sh
git clone https://gitlab.com/alelopez/boilerplate.git
cd boilerplate
npm install
```

For starting the boilerplate just execute gulp inside the boilerplate directory:
```sh
gulp
```
